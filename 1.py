import pandas as pd
import matplotlib.pyplot as plt
from scipy import stats

total  = pd.read_csv("data.csv", header = 0, sep = ",")

x = total["Price"]
y = total["Units_Sold"]

slope, intercept, r, p, std_err = stats.linregress(x, y)

def myfunc(x):
    return slope * x + intercept

mymodel = list(map(myfunc, x))

plt.scatter(x, y)
plt.plot(x, mymodel)
plt.ylim(ymin=0, ymax = 1100)
plt.xlim(xmin=0, xmax = 80)
plt.xlabel = ("Price")
plt.ylabel = ("Units_Sold")


