import pandas as pd
import statsmodels.formula.api as smf

total = pd.read_csv("data.csv", header = 0, sep=",")

model = smf.ols('Units Sold ~ Price', data = total)
results = model.fit()
print(results.summary())