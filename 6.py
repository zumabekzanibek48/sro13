import pandas as pd 
import matplotlib.pyplot as plt 
from scipy import stats 
total = pd.read_csv("data.csv", header=0, sep=",")

x = total["price"]
y = total["units sold"]

slope, intercept, r, p, std_err = stats.linregress(x, y)

def myfunc(x): 
    return slope * x + intercept
mymodel = list(map(myfunc, x))
print(mymodel) 

plt.scatter(x, y)
plt.plot(x, mymodel)
plt.ylim(ymin=0, ymax=1100)
plt.xlim(xmin=0, xmax=110)
plt.xlabel("price") 
plt.ylabel ("units_sold") 

plt.show()